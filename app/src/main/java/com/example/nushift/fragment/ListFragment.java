package com.example.nushift.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nushift.APIInterface;

import com.example.nushift.APIclient;
import com.example.nushift.CityModel;
import com.example.nushift.MainActivity;
import com.example.nushift.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    ProgressDialog progressDialog;
    FloatingActionButton mAddFab;
    List<CityModel> cityModelList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Cities List");
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        mAddFab = view.findViewById(R.id.add_fab);
        mAddFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddCityFragment fragment = new AddCityFragment();
                ((MainActivity) getActivity()).addFragmentToList(fragment);

            }
        });
        getData();



        return view;
    }

    private void getData() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMax(100);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        apiInterface = APIclient.getClient().create(APIInterface.class);
        Call<List<CityModel>> call = apiInterface.GetCitiesList();
        call.enqueue(new Callback<List<CityModel>>() {
            @Override
            public void onResponse(Call<List<CityModel>> call, Response<List<CityModel>> response) {
                progressDialog.dismiss();

                Log.d("TAG", "onResponse: "+response.raw().toString());

                List<CityModel> list = response.body();
                cityModelList.clear();
                cityModelList.addAll(((MainActivity) getActivity()).citylist);
                cityModelList.addAll(list);
                CityListAdapter adapter = new CityListAdapter(cityModelList);
                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<CityModel>> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("TAG", "error: "+t.toString());
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Cities List");

    }

    private class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.MyViewHolder>{
        List<CityModel> citylist;
        public CityListAdapter(List<CityModel> cityModelList) {
            citylist= cityModelList;

        }

        @NonNull
        @Override
        public CityListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater= (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.list_layout, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CityListAdapter.MyViewHolder holder, final int position) {
            holder.titleText.setText(citylist.get(position).getName());
            holder.descText.setText(citylist.get(position).getState());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ViewFragment fragment = new ViewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("city",citylist.get(position).getName());
                    bundle.putString("state",citylist.get(position).getState());
                    bundle.putString("lon",citylist.get(position).getLon());
                    bundle.putString("lat",citylist.get(position).getLat());
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).addFragmentToList(fragment);

                }
            });
        }

        @Override
        public int getItemCount() {
            return citylist.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView titleText,descText;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                titleText = itemView.findViewById(R.id.titleText);
                descText = itemView.findViewById(R.id.decrText);
            }
        }
    }
}