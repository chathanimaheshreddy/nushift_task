package com.example.nushift.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.nushift.CityModel;
import com.example.nushift.MainActivity;
import com.example.nushift.R;


public class AddCityFragment extends Fragment {
        View view;
        EditText city,state,lon, lat;
        Button save;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Add Item");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_add_city, container, false);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);
        lon = view.findViewById(R.id.lon);
        lat = view.findViewById(R.id.lat);
        save = view.findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (city.getText().toString().isEmpty()){
                    city.setError("City Required");
                    city.requestFocus();
                }else if (state.getText().toString().isEmpty()){
                    state.setError("State Required");
                    state.requestFocus();
                }else if (lat.getText().toString().isEmpty()){
                    lat.setError("Latitude Required");
                    lat.requestFocus();
                }else if (lon.getText().toString().isEmpty()){
                    lon.setError("Longitude Required");
                    lon.requestFocus();
                }else {
                    CityModel model = new CityModel();
                    model.setLat(lat.getText().toString());
                    model.setLon(lon.getText().toString());
                    model.setName(city.getText().toString());
                    model.setState(state.getText().toString());
                    ((MainActivity) getActivity()).citylist.add(model);
                    ((MainActivity) getActivity()).onBackPressed();
                }

            }
        });
        return view;
    }
}
