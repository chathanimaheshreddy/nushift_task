package com.example.nushift.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.nushift.MainActivity;
import com.example.nushift.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class ViewFragment extends Fragment implements OnMapReadyCallback {
    View view;
    private GoogleMap mMap;
    String city = "";
    String state = "";
    String lat = "";
    String lon = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            city = bundle.getString("city");
            state = bundle.getString("state");
            lon = bundle.getString("lon");
            lat = bundle.getString("lat");

        }
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Details View");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.fragment_view, container, false);
        TextView citytext=view.findViewById(R.id.city);
        TextView statetext=view.findViewById(R.id.state);
        TextView lattext=view.findViewById(R.id.lat);
        TextView lontext=view.findViewById(R.id.lon);
        citytext.setText(city);
        statetext.setText(state);
        lattext.setText(lat);
        lontext.setText(lon);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latlang = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
        mMap.addMarker(new
                MarkerOptions().position(latlang).title(city));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latlang));
    }
}