package com.example.nushift.kotlin

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {

        val query = ("CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY, " +
                NAME_COl + " TEXT," +
                AGE_COL + " TEXT" + ")")
        db.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun addName(name : String, age : String ){
        val values = ContentValues()
        values.put(NAME_COl, name)
        values.put(AGE_COL, age)
        val db = this.writableDatabase
        db.insert(TABLE_NAME, null,values)
        db.close()
    }
    fun updateage(name : String, age : String ){
        val values = ContentValues()
        values.put(NAME_COl, name)
        values.put(AGE_COL, age)
        val db = this.writableDatabase
      //  db.update(TABLE_NAME, values, AGE_COL, arrayOf(name))
        db.update(TABLE_NAME, values, "$NAME_COl=?", arrayOf(name));
        db.close()
    }
    fun getName(): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME", null)

    }
    fun checkname(name : String): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $NAME_COl = '$name'", null)
    }
    fun checklogin(name : String,age : String): String? {
        val db = this.readableDatabase
        val cursor =  db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $NAME_COl = '$name'", null)
        if(cursor!=null && cursor.getCount()>0){
            val cursor1 =  db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $NAME_COl = '$name' AND $AGE_COL = '$age'", null)
            if(cursor1!=null && cursor1.getCount()>0){
                return "Login Success";
            }else{
                return "Invalid Username or Password!"
            }
        }else{
            return "User not available!"
        }

    }

    companion object{
        private val DATABASE_NAME = "goldstone"
        private val DATABASE_VERSION = 1
        val TABLE_NAME = "user_table"
        val ID_COL = "id"
        val NAME_COl = "name"
        val AGE_COL = "age"
    }
}