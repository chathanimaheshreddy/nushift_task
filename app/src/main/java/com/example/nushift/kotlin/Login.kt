package com.example.nushift.kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.text.TextUtils
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.nushift.MainActivity
import com.example.nushift.R

class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login1)
        var loginbtn:Button = findViewById(R.id.loginbtn)
        var email:EditText = findViewById(R.id.email)
        var password:EditText = findViewById(R.id.password)
        var register:TextView = findViewById(R.id.register)

        loginbtn.setOnClickListener {
            var emailtxt: String
            var passwordtxt: String

            if (email.text.length == 0){
                email.setError("Enter email")
                Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG)
            }else if (password.text.length == 0){
                password.setError("Enter password")
                Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG)
            }else if (!isValidEmail(email.toString())){
                email.setError("Enter valid email")
                Toast.makeText(this,"Please enter valid email",Toast.LENGTH_LONG)
            }else if (!isValidPassword(password.toString())){
                password.setError("Enter valid password")
                Toast.makeText(this,"Please enter valid password",Toast.LENGTH_LONG)
            }else{
                userLogin(email.toString(),password.toString())
            }
        }
    }

    private fun userLogin(useremail: String, password: String) {
        val db = DBHelper(this, null)
        var msg = db.checklogin(useremail,password)
        if (msg != null && msg != ""){
            if (msg.equals("Login Success")){
               /* val intent = Intent()
                intent.setClass(this,MainActivity::class.java)
                startActivity(intent)*/
            }
            Toast.makeText(this,msg,Toast.LENGTH_LONG)
        }else{
            Toast.makeText(this,"Unable proccess the request now",Toast.LENGTH_LONG)
        }
    }

    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
    internal fun isValidPassword(password: String): Boolean {
        if (password.length < 8) return false
        if (password.filter { it.isDigit() }.firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isUpperCase() }.firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isLowerCase() }.firstOrNull() == null) return false
        if (password.filter { !it.isLetterOrDigit() }.firstOrNull() == null) return false

        return true
    }
    /* addName.setOnClickListener{
            val name = enterName.text.toString()
            val age = enterAge.text.toString()
            val db = DBHelper(this, null)
           /* val cursor = db.checkname(name)
            if(cursor!=null && cursor.getCount()>0){
                var list : List<String> = ArrayList<String>();
                while (cursor.moveToNext()) {

                    Log.d("TAG", "onCreate:name- "+cursor.getString(cursor.getColumnIndex(DBHelper.NAME_COl))+"    age-  "
                            + cursor.getString(cursor.getColumnIndex(DBHelper.AGE_COL)))
                }
                Toast.makeText(this, name + " already exists", Toast.LENGTH_LONG).show()

            }else{
                db.addName(name, age)

                // Toast to message on the screen
                Toast.makeText(this, name + " added to database", Toast.LENGTH_LONG).show()
                enterName.text.clear()
                enterAge.text.clear()
            }*/
            /*db.updateage(name, age)

            // Toast to message on the screen
            Toast.makeText(this, name + " updated to database", Toast.LENGTH_LONG).show()
            enterName.text.clear()
            enterAge.text.clear()*/
            val cursor = db.checklogin(name,age)
            if(cursor!=null && !cursor.equals("")){
                Toast.makeText(this, cursor , Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this,  " unable to process", Toast.LENGTH_LONG).show()
            }

        }

        // below code is to add on  click
        // listener to our print name button
        printName.setOnClickListener{
            val db = DBHelper(this, null)
            val cursor = db.getName()
            if(cursor!=null && cursor.getCount()>0) {
                while (cursor.moveToNext()) {

                    Log.d(
                        "TAG",
                        "onCreate:name- " + cursor.getString(cursor.getColumnIndex(DBHelper.NAME_COl)) + "    age-  "
                                + cursor.getString(cursor.getColumnIndex(DBHelper.AGE_COL))
                    )
                }
            }
            cursor!!.close()
        }*/
}